version: "3"

volumes:
  postgres_data:
  db-data:
  wait-tool:
  
services:
  mailhog:
    image: mailhog/mailhog
    container_name: mailhog
    ports:
      - ${SMTP_PORT}:1025
      - 8025:8025

  postgres:
    image: postgres:12.0
    container_name: postgres
    restart: always    
    volumes:
      - postgres_data:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: ${POSTGRES_DB}
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
    ports:
      - "${POSTGRES_PORT}:5432"
  
  keycloak:
    image: jboss/keycloak:7.0.0
    container_name: keycloak
    environment:
      DB_VENDOR: ${DB_VENDOR}
      DB_ADDR: ${DB_ADDR}
      DB_DATABASE: ${DB_DATABASE}
      DB_USER: ${DB_USER}
      DB_PASSWORD: ${DB_PASSWORD}
      KEYCLOAK_USER: ${KEYCLOAK_USER}
      KEYCLOAK_PASSWORD: ${KEYCLOAK_PASSWORD}
    ports:
      - "${KEYCLOAK_PORT}:8080"
    depends_on:
      - postgres

  nsiws-design:
    image: siscc/dotstatsuite-core-sdmxri-nsi:master
    container_name: nsiws-design
    ports:
      - "${NSI_DESIGN_PORT}:80"
    environment:
        auth__Enabled: "true"
        auth__authority: http://keycloak:${KEYCLOAK_PORT}/auth/realms/${KEYCLOAK_REALM}
        auth__clientId: ${DOTSTAT_CLIENT}
        auth__requireHttps: "false"
        auth__validateIssuer: "false" 
        auth__claimsMapping__email: email
        auth__claimsMapping__groups: groups
        auth__allowAnonymous: "true"
        DotStatSuiteCoreCommonDbConnectionString: Server=db;Database=${COMMON_DB};User=${COMMON_DB_USER};Password=${COMMON_DB_PWD};
        SpacesInternal__0__Id: design
        SpacesInternal__0__DotStatSuiteCoreStructDbConnectionString: Server=db;Database=${STRUCT_DB_DESIGN};User=${STRUCT_DB_DESIGN_USER};Password=${STRUCT_DB_DESIGN_PWD};            
        SpacesInternal__0__DotStatSuiteCoreDataDbConnectionString: Server=db;Database=${DATA_DB_DESIGN};User=${DATA_DB_DESIGN_USER};Password=${DATA_DB_DESIGN_PWD};
        SpacesInternal__0__DataImportTimeOutInMinutes: 2
        SpacesInternal__0__DatabaseCommandTimeoutInSec: 60   
        SpacesInternal__0__AuthEndpoint: auth-service
        SpacesInternal__0__AutoLog2DB: "true"
        SpacesInternal__0__AutoLog2DBLogLevel: "Notice"
        SQL_SERVER: db
        SQL_DATABASE: ${STRUCT_DB_DESIGN}
        SQL_USER: ${STRUCT_DB_DESIGN_USER}
        SQL_PASSWORD: ${STRUCT_DB_DESIGN_PWD}      
        SENDER_ID: "Design - DotStat v8"
        DATASPACE_ID: design            
        MA_DB_VERSION: 6.7
        MA_SQL_USER: sa
        MA_SQL_PASSWORD: ${SA_PASSWORD}
        #Persist
        MA_ALWAYS_RESET: N
        INSERT_NEW_ITEM_SCHEME_VALUES: "false"
    depends_on: 
      - auth-service
      - dbup-struct-design
    volumes:
      - "./logs:/app/logs" 
      - wait-tool:/tool
    entrypoint: bash -c "/tool/wait4sql db '${STRUCT_DB_DESIGN_USER}' '${STRUCT_DB_DESIGN_PWD}' '${STRUCT_DB_DESIGN}' && ./entrypoint.sh" 
    
  nsiws-disseminate:
    image: siscc/dotstatsuite-core-sdmxri-nsi:master
    container_name: nsiws-disseminate
    ports:
      - "${NSI_DISSEMINATE_PORT}:80"
    environment:
        auth__Enabled: "true"
        auth__authority: http://keycloak:${KEYCLOAK_PORT}/auth/realms/${KEYCLOAK_REALM}
        auth__clientId: ${DOTSTAT_CLIENT}
        auth__requireHttps: "false"
        auth__validateIssuer: "false" 
        auth__claimsMapping__email: email
        auth__claimsMapping__groups: groups
        auth__allowAnonymous: "true"
        DotStatSuiteCoreCommonDbConnectionString: Server=db;Database=${COMMON_DB};User=${COMMON_DB_USER};Password=${COMMON_DB_PWD};
        SpacesInternal__0__Id: diseminate
        SpacesInternal__0__DotStatSuiteCoreStructDbConnectionString: Server=db;Database=${STRUCT_DB_DISSEMINATE};User=${STRUCT_DB_DISSEMINATE_USER};Password=${STRUCT_DB_DISSEMINATE_PWD};            
        SpacesInternal__0__DotStatSuiteCoreDataDbConnectionString: Server=db;Database=${DATA_DB_DISSEMINATE};User=${DATA_DB_DISSEMINATE_USER};Password=${DATA_DB_DISSEMINATE_PWD};
        SpacesInternal__0__DataImportTimeOutInMinutes: 2
        SpacesInternal__0__DatabaseCommandTimeoutInSec: 60   
        SpacesInternal__0__AuthEndpoint: auth-service
        SpacesInternal__0__AutoLog2DB: "true"
        SpacesInternal__0__AutoLog2DBLogLevel: "Notice"
        SQL_SERVER: db
        SQL_DATABASE: ${STRUCT_DB_DISSEMINATE}
        SQL_USER: ${STRUCT_DB_DISSEMINATE_USER}
        SQL_PASSWORD: ${STRUCT_DB_DISSEMINATE_PWD}      
        SENDER_ID: "Disseminate - DotStat v8"
        DATASPACE_ID: design           
        MA_DB_VERSION: 6.7
        MA_SQL_USER: sa
        MA_SQL_PASSWORD: ${SA_PASSWORD}
        #Persist
        MA_ALWAYS_RESET: N
        INSERT_NEW_ITEM_SCHEME_VALUES: "false"
    depends_on: 
      - auth-service
      - dbup-struct-disseminate
    volumes:
      - "./logs:/app/logs" 
      - wait-tool:/tool
    entrypoint: bash -c "/tool/wait4sql db '${STRUCT_DB_DISSEMINATE_USER}' '${STRUCT_DB_DISSEMINATE_PWD}' '${STRUCT_DB_DISSEMINATE}' && ./entrypoint.sh" 
        
  transfer-service:
      image: siscc/dotstatsuite-core-transfer:master
      container_name: transfer-service
      environment:
          auth__Enabled: "true"
          auth__authority: http://keycloak:${KEYCLOAK_PORT}/auth/realms/${KEYCLOAK_REALM}
          auth__clientId: ${DOTSTAT_CLIENT}
          auth__authorizationUrl: http://keycloak:${KEYCLOAK_HTTP_PORT}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/auth
          auth__scopes__0: openid
          auth__scopes__1: profile
          auth__scopes__2: email
          auth__claimsMapping__email: email
          auth__claimsMapping__groups: groups
          auth__requireHttps: "false"
          auth__validateIssuer: "true"
          MaxTransferErrorAmount: 0 
          DefaultLanguageCode: en
          SmtpHost: ${SMTP_HOST}
          SmtpPort: ${SMTP_PORT}
          SmtpEnableSsl: ${SMTP_SSL}
          SmtpUserName: ${SMTP_USER}
          SmtpUserPassword: ${SMTP_PASSWORD}
          MailFrom: noreply@dlm.org
          MaxTextAttributeLength: 150
          DotStatSuiteCoreCommonDbConnectionString: Server=db;Database=${COMMON_DB};User=${COMMON_DB_USER};Password=${COMMON_DB_PWD};
          SpacesInternal__0__Id: design
          SpacesInternal__0__DotStatSuiteCoreStructDbConnectionString: Server=db;Database=${STRUCT_DB_DESIGN};User=${STRUCT_DB_DESIGN_USER};Password=${STRUCT_DB_DESIGN_PWD};
          SpacesInternal__0__DotStatSuiteCoreDataDbConnectionString: Server=db;Database=${DATA_DB_DESIGN};User=${DATA_DB_DESIGN_USER};Password=${DATA_DB_DESIGN_PWD}; 
          SpacesInternal__0__DataImportTimeOutInMinutes: 2
          SpacesInternal__0__DatabaseCommandTimeoutInSec: 60
          SpacesInternal__0__AutoLog2DB: "true"
          SpacesInternal__0__AutoLog2DBLogLevel: "Notice"
          SpacesInternal__1__Id: disseminate
          SpacesInternal__1__DotStatSuiteCoreStructDbConnectionString: Server=db;Database=${STRUCT_DB_DISSEMINATE};User=${STRUCT_DB_DISSEMINATE_USER};Password=${STRUCT_DB_DISSEMINATE_PWD};
          SpacesInternal__1__DotStatSuiteCoreDataDbConnectionString: Server=db;Database=${DATA_DB_DISSEMINATE};User=${DATA_DB_DISSEMINATE_USER};Password=${DATA_DB_DISSEMINATE_PWD}; 
          SpacesInternal__1__DataImportTimeOutInMinutes: 2
          SpacesInternal__1__DatabaseCommandTimeoutInSec: 60
          SpacesInternal__1__AutoLog2DB: "true"
          SpacesInternal__1__AutoLog2DBLogLevel: "Notice"
      volumes:
        - "./logs:/app/logs" 
      ports:
        - "${TRANSFER_PORT}:80"
      depends_on: 
        - auth-service
        - dbup-data-design
        - dbup-struct-design
        - dbup-data-disseminate
        - dbup-struct-disseminate
            
  auth-service:
      image: siscc/dotstatsuite-core-auth-management:master
      container_name: auth-service
      environment:
          auth__enabled: "true"
          auth__authority: http://keycloak:${KEYCLOAK_PORT}/auth/realms/${KEYCLOAK_REALM}
          auth__clientId: ${DOTSTAT_CLIENT}
          auth__authorizationUrl: http://keycloak:${KEYCLOAK_HTTP_PORT}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/auth
          auth__scopes__0: openid
          auth__scopes__1: profile
          auth__scopes__2: email
          auth__claimsMapping__email: email
          auth__claimsMapping__groups: groups
          auth__requireHttps: "false"
          auth__validateIssuer: "true"
          DotStatSuiteCoreCommonDbConnectionString: Server=db;Database=${COMMON_DB};User=${COMMON_DB_USER};Password=${COMMON_DB_PWD};
      volumes:
        - "./logs:/app/logs" 
      ports:
        - "${AUTH_PORT}:80"
      depends_on: 
        - dbup-common

  dbup-struct-design:
      image: siscc/dotstatsuite-dbup:master
      container_name: dbup-struct-design
      environment:
          EXECUTION_PARAMETERS: upgrade --connectionString Server=db;Database=${STRUCT_DB_DESIGN};User=sa;Password=${SA_PASSWORD}; --mappingStoreDb --loginName ${STRUCT_DB_DESIGN_USER} --loginPwd ${STRUCT_DB_DESIGN_PWD} --force 
      depends_on: 
        - db
      volumes:
        - wait-tool:/tool
      entrypoint: bash -c "/tool/wait4sql db sa ${SA_PASSWORD} && dotnet DotStat.DbUp.dll $$EXECUTION_PARAMETERS"
      
  dbup-data-design:
      image: siscc/dotstatsuite-dbup:master
      container_name: dbup-data-design
      environment:
          EXECUTION_PARAMETERS: upgrade --connectionString Server=db;Database=${DATA_DB_DESIGN};User=sa;Password=${SA_PASSWORD}; --dataDb --loginName ${DATA_DB_DESIGN_USER} --loginPwd ${DATA_DB_DESIGN_PWD} --force 
      depends_on: 
        - db
      volumes:
        - wait-tool:/tool
      entrypoint: bash -c "/tool/wait4sql db sa ${SA_PASSWORD} && dotnet DotStat.DbUp.dll $$EXECUTION_PARAMETERS"

  dbup-struct-disseminate:
      image: siscc/dotstatsuite-dbup:master
      container_name: dbup-struct-default
      environment:
          EXECUTION_PARAMETERS: upgrade --connectionString Server=db;Database=${STRUCT_DB_DISSEMINATE};User=sa;Password=${SA_PASSWORD}; --mappingStoreDb --loginName ${STRUCT_DB_DISSEMINATE_USER} --loginPwd ${STRUCT_DB_DISSEMINATE_PWD} --force 
      depends_on: 
        - db
      volumes:
        - wait-tool:/tool
      entrypoint: bash -c "/tool/wait4sql db sa ${SA_PASSWORD} && dotnet DotStat.DbUp.dll $$EXECUTION_PARAMETERS"
      
  dbup-data-disseminate:
      image: siscc/dotstatsuite-dbup:master
      container_name: dbup-data-default
      environment:
          EXECUTION_PARAMETERS: upgrade --connectionString Server=db;Database=${DATA_DB_DISSEMINATE};User=sa;Password=${SA_PASSWORD}; --dataDb --loginName ${DATA_DB_DISSEMINATE_USER} --loginPwd ${DATA_DB_DISSEMINATE_PWD} --force 
      depends_on: 
        - db
      volumes:
        - wait-tool:/tool
      entrypoint: bash -c "/tool/wait4sql db sa ${SA_PASSWORD} && dotnet DotStat.DbUp.dll $$EXECUTION_PARAMETERS"
        
  dbup-common:
      image: siscc/dotstatsuite-dbup:master
      container_name: dbup-common
      depends_on: 
        - db
      environment:
          EXECUTION_PARAMETERS: upgrade --connectionString Server=db;Database=${COMMON_DB};User=sa;Password=${SA_PASSWORD}; --commonDb --loginName ${COMMON_DB_USER} --loginPwd ${COMMON_DB_PWD} --force          
      volumes:
        - wait-tool:/tool          
      entrypoint: bash -c "/tool/wait4sql db sa ${SA_PASSWORD} && dotnet DotStat.DbUp.dll $$EXECUTION_PARAMETERS"
  
  db:
      image: mcr.microsoft.com/mssql/server:2017-latest-ubuntu 
      container_name: mssql
      ports:
        - "${SQL_PORT}:1433"
      environment:
        - SA_PASSWORD=${SA_PASSWORD}
        - MSSQL_PID=Developer
        - ACCEPT_EULA=Y
      depends_on: 
        - wait  
      volumes:
        - "db-data:/var/opt/mssql/data"          

  wait: 
    image: vigurous/wait4sql
    container_name: wait4sql
    volumes:
      - wait-tool:/tool
